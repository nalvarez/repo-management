#!/bin/sh

# Adapted from the "fork" admin-defined command from Gitolite.
# Modified to ensure the base path remains constant

. $(dirname $0)/adc.common-functions

[ -z "$GL_RC" ] && die "ENV GL_RC not set"

get_rights_and_owner $1; from=$repo
[ -z "$perm_read" ] && die "no read permissions on $repo"

to=$2

[ -z "$from" -o -z "$to" ] && die "This command takes two parameters (from and to)!"

dirup=".."
[[ "$from" =~ "${dirup}" ]] && die "You are not allowed to traverse to parent directories."
[[ "$to" =~ "${dirup}" ]] && die "You are not allowed to traverse to parent directories."

oldIFS=$IFS
IFS="/"
fromarray=( $from )
toarray=( $to )
IFS=$oldIFS

if [ ${#toarray[@]} -gt 1 ]; then
    die "The destination takes a single path element -- the name. The location is chosen automatically."
fi

if [ ${#fromarray[@]} -eq 1 ]; then
    to="clones/$from/$GL_USER/$to"
elif [[ ${fromarray[0]} == "scratch" ]]; then
    if [ ${#fromarray[@]} -ne 3 ]; then
        die "Incorrect source location."
    fi
    from="scratch/${fromarray[1]}/${fromarray[2]}"
    to="scratch/$GL_USER/$to"
elif [[ ${fromarray[0]} == "clones" ]]; then
    from="clones/${fromarray[1]}/${fromarray[2]}/${fromarray[3]}"
    to="clones/${fromarray[1]}/$GL_USER/$to"
elif [[ ${fromarray[0]} == "websites" ]]; then
    from="websites/${fromarray[1]}"
    to="clones/websites/${fromarray[1]}/$GL_USER/$to"
else
    die "Not valid source/destination locations"
fi

get_rights_and_owner $to; to=$repo

echo "Cloning, please be patient..."

# clone $from to $to
git clone --bare -l $GL_REPO_BASE_ABS/$from.git $GL_REPO_BASE_ABS/$to.git > /dev/null
[ $? -ne 0 ] && exit 1

echo "New clone of $from created at $to"

# fix up creator, gitweb owner, and hooks
cd $GL_REPO_BASE_ABS/$to.git
echo $GL_USER > gl-creater
git config gitweb.owner "$GL_USER"
( $GL_BINDIR/gl-query-rc GL_WILDREPOS_DEFPERMS ) |
    SSH_ORIGINAL_COMMAND="setperms $to" $GL_BINDIR/gl-auth-command $GL_USER
cp -R $GL_REPO_BASE_ABS/$from.git/hooks/* $GL_REPO_BASE_ABS/$to.git/hooks
touch git-daemon-export-ok

if [ -n "$GL_WILDREPOS_DEFPERMS" ]; then
  echo "$GL_WILDREPOS_DEFPERMS" > gl-perms
fi

echo "$from" > kde-cloned-from

echo "" > description

currpath=$(pwd)
urlpath=${currpath#/srv/kdegit/repositories/}
urlpath=${urlpath%.git}
cat > cloneurl << EOF
Pull (read-only): git://anongit.kde.org/$urlpath
Pull (read-only): http://anongit.kde.org/$urlpath
Pull+Push (read+write): git@git.kde.org:$urlpath
EOF

# run gitolite's post-init hook if you can (hook code expects GL_REPO to be set)
export GL_REPO; GL_REPO="$to"
[ -x hooks/gl-post-init ] && hooks/gl-post-init
